Add-Type -AssemblyName PresentationFramework
$strMessage = 'Would you like to reboot now?  All of your data will be cleared from this computer upon reboot.'
$msgBoxInput = [System.Windows.MessageBox]::Show($strMessage,'Reboot','YesNo','Question')
switch ($msgBoxInput) {
    'Yes' {
        Restart-Computer
    }
    'No' {
        break
    }
}