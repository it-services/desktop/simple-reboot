# Simple Reboot
This is a simple PowerShell script which presents a user with a GUI prompt to confirm or deny a system reboot

## Getting Started
Run this script to present the user with a reboot prompt.  You can also compile the script into an EXE using PS2EXE.
